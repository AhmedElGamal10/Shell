#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <memory.h>
#include <errno.h>

#define MAX_LINE 80 /* The maximum length command */

const int BATCH_MODE = 1, INTERACTIVE_MODE = 2, DYNAMIC_ARR_LEN = 1000;
int mode;
int tokens_number;
char **batch_file;//[DYNAMIC_ARR_LEN][MAX_LINE];

bool equal(char a[], char b[]) {
    if (strcmp(a, b) == 0)
        return 1;
    else
        return 0;
}

char **split(char *input, char delimiter) {
    char **result;
    result = (char **) malloc(DYNAMIC_ARR_LEN * (sizeof(char *)));
    memset(result, '\0', DYNAMIC_ARR_LEN * (sizeof(char *)));
    int i;
    for (i = 0; i < DYNAMIC_ARR_LEN; i++) {
        result[i] = (char *) malloc(MAX_LINE * (sizeof(char)));
        memset(result[i], '\0', MAX_LINE * (sizeof(char)));
    }

    tokens_number = 0;
    i = 0;
    for (i = 0; i < strlen(input); i++) {
        char current = *(input + i);
        if (current == delimiter) {
            continue;
        } else {
            int j = 0;
            while (*(input + i) != delimiter && *(input + i) != NULL) {
                *(*(result + tokens_number) + j) = *(input + i);
                i++;
                j++;
            }
            tokens_number++;
        }
    }

    return result;
}

int to_number(char *number_in_chars) {
    if (strlen(number_in_chars) == 1) {
        return number_in_chars[0] - '0';
    }
    int dec = 1;
    int i;
    for (i = 0; i < strlen(number_in_chars); i++) {
        dec = dec * 10 + (number_in_chars[i] - '0');
    }
    return dec;
}

char* get_next_command(int batch_file_counter) {
    return batch_file[batch_file_counter];
}

char *get_full_command(char *command) {
    int i;
    tokens_number = 0;

    char *PATH = getenv("PATH");
    char **possible_paths = split(PATH, ':');

    for (i = 0; i < tokens_number; i++) {
        possible_paths[i][strlen(possible_paths[i])] = '/';
        char *full_command = strcat(possible_paths[i], command);
        int result = access(full_command, F_OK);
        if (result == 0) {
            return possible_paths[i];
        }
    }
    return "";
}


void read_batch_file(char *file_path) {
    FILE *file = fopen(file_path, "r");

    batch_file = (char **) malloc(DYNAMIC_ARR_LEN * (sizeof(char *)));
    memset(batch_file, '\0', DYNAMIC_ARR_LEN * (sizeof(char *)));

    int i;
    for (i = 0; i < 1000; i++) {
        batch_file[i] = (char *) malloc(MAX_LINE * 2 * (sizeof(char)));
        memset(batch_file[i], '\0', MAX_LINE * 2 * (sizeof(char)));
    }
    int batch_file_counter = 0;
    if (file != NULL) {
        char* line = (char *)malloc(MAX_LINE * 2 *sizeof(char));
        while (fgets(line, sizeof(line), file) != NULL) /* read a line */
        {
            strcpy(batch_file[batch_file_counter],line);
            batch_file_counter++;
        }
        fclose(file);
    }
}


void process(int mode, char *file_path) {
    static char *input;
    static char history[10000][MAX_LINE];
    int should_run = 1;
    int batch_file_counter = 0;
    int history_counter = 0;
    input = (char *) malloc(MAX_LINE * (sizeof(char)));
    FILE *filereaderBat = fopen(file_path, "r");

    if(mode == BATCH_MODE){
        read_batch_file(file_path);
    }

    while (should_run) {

        if (mode == INTERACTIVE_MODE) {
            printf("SHELL> ");
            fflush(stdout);
            fgets(input, 1000, stdin);
            input[strlen(input) - 1] = NULL;

        } else if (mode == BATCH_MODE) {
//            if(fgets(input, 1000, filereaderBat) == NULL) break;
            char step_input;
            scanf("%c",&step_input);

            if(step_input == NULL)
                break;

            input = get_next_command(batch_file_counter);
            batch_file_counter++;
            if (input[strlen(input) - 1] == '\n') {
                input[strlen(input) - 1] = '\0';
            }

            printf("%s",input);

        }

        //empty line
        if (strlen(input) < 2) {
            continue;
        }

        if (equal(input, "Exit") || equal(input, "exit")) {
            break;
        }

        char *cut_input = (char *) malloc(MAX_LINE * (sizeof(char)));
        int i;
        for (i = 0; i < strlen(input) - 1; i++) {
            cut_input[i] = input[i];
        }

        if (strlen(input) > MAX_LINE)
            printf("Error too long command!");

        //dealing with history
        if (equal(input, "history")) {
            int i;
            for (i = history_counter - 1; i >= history_counter - 10; i--) {
                if (i < 0)
                    break;
                printf("history is: %s\n", history[i]);
            }
            continue;
        } else if (equal(input, "!!")) {
            if (history_counter > 0)
                printf("%s\n", history[history_counter - 1]);
            else
                printf("No commands in history.\n");
            continue;
        } else if (input[0] == '!' && strlen(input) > 1) {

            char *number_in_chars = (char *) malloc(DYNAMIC_ARR_LEN * sizeof(char));

            for (i = 1; i < strlen(input); i++) {
                number_in_chars[i - 1] = input[i];
            }
            int number = to_number(number_in_chars);
            if (number > 10 || number < 0 || (history_counter - number) < 0) {
                printf("No such command in history.\n");
            } else {
                printf("%s\n", history[history_counter - number]);
            }
            continue;
        }

        //store command in history
        if(!equal(input, "!!") && !(input[0] == '!')){
            for (i = 0; i < strlen(input); i++) {
                history[history_counter][i] = input[i];
            }
            history[history_counter][strlen(input)] = '\0';
            history_counter++;
        }

        //prepare command for execution
        char **parsed_input = split(input, ' ');
        char *last = parsed_input[tokens_number - 1];

        bool ampersand_included = equal(last, "&");

        char *full_command;

        //handle "ls"
        if (equal(parsed_input[0], "ls") || equal(parsed_input[0], "/bin/ls") ) {
            full_command = "/bin/ls";
            parsed_input[tokens_number - 1] = "/";
            parsed_input[tokens_number] = NULL;
        }else if(access(parsed_input[0], F_OK) == 0){
            full_command = parsed_input[0];
        }else {
            full_command = get_full_command(parsed_input[0]);
        }
        parsed_input[0] = full_command;

        //process part
        pid_t pid;
        if ((pid = fork()) == -1)
            perror("fork error");
        else if (pid == 0) {
            execvp(full_command, parsed_input);
            printf("execv failed with error %d %s\n", errno, strerror(errno));
        } else {
            if (!ampersand_included)
                waitpid(pid, NULL, 1);
        }
    }
}

int main(int argc, char *argv[]) {
    mode = INTERACTIVE_MODE;
    char *file_path = "/home/ahmedelgamal/Desktop/text";
    mode = BATCH_MODE;

    if (argc == 2 && !equal(argv[1], "ls")) {
        mode = BATCH_MODE;
        file_path = argv[1];
    }

    process(mode, file_path);
    return 0;
}